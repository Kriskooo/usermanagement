﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebProject.Services.Interfaces;
using WebProject.Services.RequestModels;
using WebProject.Services.Services;

namespace WebProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterController : ControllerBase
    {
        private readonly IRegisterService _registerService;
        public RegisterController(RegisterService registerService)
        {
            _registerService = registerService;
        }

        [HttpPost("register")]
        public async Task<ActionResult> RegisterUserAsync(RegisterUser request)
        {
            IdentityResult? result = await _registerService.RegisterUserAsync(request);

            if (!result.Succeeded)
            {
                return BadRequest(result.Errors);
            }

            return CreatedAtAction(null, null);
        }

        [HttpGet("username")]
        public async Task<ActionResult> GetUsersAsync(string username)
        {
            var profile = await _registerService.GetUsersAsync(username);

            return Ok(profile);
        }
    }
}
