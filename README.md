Requirements:

• Setup: Use EF Core with the in-memory database provider.
• Data Model: Create a User entity with properties like Id, Username, and
Email.
• Endpoint to Register Users: Create an API endpoint to register (add) new
users.
• Endpoint to List Users: Create an endpoint to fetch a list of all registered users.