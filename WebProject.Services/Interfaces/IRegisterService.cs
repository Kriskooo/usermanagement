﻿using Microsoft.AspNetCore.Identity;
using WebProject.Services.RequestModels;

namespace WebProject.Services.Interfaces
{
    public interface IRegisterService
    {
        Task<IdentityResult> RegisterUserAsync(RegisterUser request);
        Task<UserDto> GetUsersAsync(string username);
    }
}
