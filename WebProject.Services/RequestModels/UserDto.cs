﻿namespace WebProject.Services.RequestModels
{
    public class UserDto
    {
        public string Username { get; set; }
        public string Email { get; set; }
    }
}
