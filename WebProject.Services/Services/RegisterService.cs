﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using WebProject.Database;
using WebProject.Services.Interfaces;
using WebProject.Services.RequestModels;

namespace WebProject.Services.Services
{
    public class RegisterService : IRegisterService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _context;
        public RegisterService(UserManager<ApplicationUser> userManager, ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        public async Task<IdentityResult> RegisterUserAsync(RegisterUser request)
        {
            var user = new ApplicationUser() { Username = request.UserName, Email = request.Email };
            var result = await _userManager.CreateAsync(user, request.Password);
            return result;
        }

        public async Task<UserDto> GetUsersAsync(string username)
        {
            return await _context.Users.Where(applicationUser => applicationUser.Username == username).Select(applicationUser => new UserDto
            {
                Username = applicationUser.Username,
                Email = applicationUser.Email,
            }).FirstOrDefaultAsync();

        }
    }
}
