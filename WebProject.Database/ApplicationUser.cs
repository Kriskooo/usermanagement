﻿using Microsoft.AspNetCore.Identity;

namespace WebProject.Database
{
    public class ApplicationUser : IdentityUser
    {
        public string Username { get; set; }
        public string Email { get; set; }
    }
}
